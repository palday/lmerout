#' @rdname pprint
#' @export
#' @import xtable
#' @import lme4
#' @import stringr
#' @import dplyr
pprint.anova <- function(x, file = NULL, 
                        label = NULL, caption = NULL,
                        type = "latex",
                        float.spec = "htb", booktabs = TRUE, 
                        interaction.level = NA, interaction.symbol = NA, 
                        min.p = NA, max.p = NA,
                        sig.stars = TRUE, 
                        ...){
    
    h <- attr(x,"heading")
    tmp <- tempfile()
    if(is.null(caption)){
        if(type == "html"){
            caption <- paste(h, collapse="<br/>\n")
        }
        if(type == "latex"){
            caption <- paste0(h, collapse="\\newline{}")
            # replace tildes with the correct escape sequence
            caption <- gsub("~","\\textasciitilde{}",caption,fixed=TRUE)
            # escape dollar signs
            caption <- gsub("$","\\$",caption,fixed=TRUE)
            # escape underscore
            caption <- gsub("_","\\_",caption,fixed=TRUE)
        }
    }
    
    if(!is.na(interaction.level)){
        x <- x[ilevel(row.names(x)) <= interaction.level,]
    }
    
    # p-value is last column when there are at least 4 columns
    # otherwise it's missing
    if(!is.na(min.p)){
        if(ncol(x) < 4)
            warning('min.p ignored for ANOVAs without p-values')
        else
            x <- x[x[,ncol(x)] >= min.p,]
    }
    if(!is.na(max.p)){
        if(ncol(x) < 4)
            warning('max.p ignored for ANOVAs without p-values')
        else
            x <- x[x[,ncol(x)] <= max.p,]
    }
    
    tab <- as_tibble(x, rowname=NA) %>%
        mutate_if(is.numeric,
                     function(x) ifelse(abs(x) < 1, signif(x,2),round(x,1))) %>%
        mutate(` ` = row.names(x)) %>%
        relocate(` `)
    
    colnames(tab) <- sub("Df", "df", colnames(tab))
    colnames(tab) <- sub("Sum Sq", "SS", colnames(tab))
    colnames(tab) <- sub("Mean Sq", "MS", colnames(tab))
    colnames(tab) <- sub("NumDF", "ndf", colnames(tab))
    colnames(tab) <- sub("DenDF", "ddf", colnames(tab))
    
    colnames(tab) <- sub("Chi df", "df", colnames(tab))
    
    if("Df.res" %in% colnames(tab)){
        colnames(tab) <- sub("df", "ndf", colnames(tab))
        colnames(tab) <- sub("Df.res", "ddf", colnames(tab))    
    }
    
    if(type == "latex"){
        tab <- latexf(tab)
        tab <- latexchi(tab)
    } else { 
        colnames(tab) <- gsub("Chisq", "&chi;2", colnames(tab))
        colnames(tab) <- gsub("Chi", "&chi;", colnames(tab))
    }
    
    if(sig.stars){
        if(str_detect(colnames(tab)[ncol(tab)],"Pr[(]>.+[)]")){
            tab$` ` <- " "
            # fill missing values for subscripting
            tab[is.na(tab[, ncol(tab)-1]),  ncol(tab)-1] <- 2
            
            tab[tab[, ncol(tab)-1] < 0.1,   ncol(tab)] <- "."
            tab[tab[, ncol(tab)-1] < 0.05,  ncol(tab)] <- "*"
            tab[tab[, ncol(tab)-1] < 0.01,  ncol(tab)] <- "**"
            tab[tab[, ncol(tab)-1] < 0.001, ncol(tab)] <- "***"
            
            tab[tab[, ncol(tab)-1] > 1, ncol(tab)-1] <- NA
        }
    }
    
    xt <- print(xtable(tab,
                      label=label,
                      caption=caption, 
                      auto=TRUE),
                  file=tmp,
                  type=type,
                  comment=FALSE,
                  html.table.attributes="border=0",
                  math.style.negative=TRUE,
                  include.rownames=FALSE,
                  table.placement=float.spec,
                  caption.placement="top",
                  sanitize.colnames.function= function(x){x})
    # prevent asterisks from being handled as markup i
    # (well, mark_down_, but same idea)
    if(type == "html"){
        xt <- gsub("*","&#42;",xt,fixed=TRUE)
    }
    
    unlink(tmp)
    if(is.null(file)){
       xt 
    }else{
        cat(xt,file=file)
    }
}

