# lmerOut
A set of convenience functions for integrating `lme4` into a LaTeX- or HTML/Markdown-based workflow (e.g with [knitr](yihui.name/knitr/) and/or [pandoc](http://johnmacfarlane.net/pandoc/)).

# Installation
```
if(!require(devtools)){
    install.packages("devtools")
    library("devtools")
}

install_bitbucket("palday/lmerOut")
library(lmerOut)
?pprint
```

With the introduction of HTML support, the formatting for LaTeX tables changed somewhat.
Additionally, the default method for a model is now a simple table of coefficients; you must pass a model summary to get a pretty-printed version of the output of `summary()`.
If you want the old behavior, then you should install the `latex-only-api` branch:

```
if(!require(devtools)){
    install.packages("devtools")
    library("devtools")
}

install_bitbucket("palday/lmerOut",ref="latex-only-api")
library(lmerOut)
?pprint
```

# Using HTML-Output to generate Word and OpenOffice Documents
The HTML output can be used to generate MS Word (docx) or OpenOffice.org/LibreOffice (odt) output.
Unfortunately, the HTML output produced by `pprint` doesn’t always place nice with pandoc when converting directly from Markdown to non-HTML formats.
For example, using pandoc (or equivalently, RStudio's buttons) to go straight from Markdown to a Word document often doesn't work.
However, if you force an HTML intermediate step, then things work just fine. 
This also holds for OpenOffice.org/LibreOffice ODT output.
Here's an exampe of converting from RMarkdown to docx or odt via HTML:

```bash
R --vanilla --slave -e 'knitr::knit("yourfile.Rmd")'
pandoc yourfile.md -t html -o - | pandoc -f html -o yourfile.docx
pandoc yourfile.md -t html -o - | pandoc -f html -o yourfile.odt
```

Alternatively, if you're using RStudio, just generate HTML output until you're ready to share with docx-demanding demons and then convert that output:

```
pandoc yourfile.html -o yourfile.docx
```

It's an unfortunate extra step but still far less tedious and error-prone than copying and pasting R output into Word by hand.
Also, if you're using OpenOffice.org/LibreOffice and you look at the docx output, the table / column widths are all much too wide.
Everything seems to render fine in Word itself and FreeOffice TextMaker, so if you're using OO.org/LO anyway to edit/view things before sending it to someone/thing who needs docx, you should convert the Markdown to odt and then use OO.org/LO to export to docx.

# Named Contrasts for Sum Encoding
*This functionality is deprecated. Use `car::contr.Sum` and related functions for variants of the built-in R contrasts with better naming.*

Although the default encoding in R for categorcal variables is dummy coding, the hypotheses tested (*simple effects* intead of *main effects*) are often not the hypotheses of interest.[[1](http://talklab.psy.gla.ac.uk/tvw/catpred/)]
R does provide built in functions for using sum (deviation) encoding, but these coefficient names for the resulting contrasts are far from transparent. To help make the interpretation of model coefficients easier, the convenience funciton `named.contr.sum()` is provided as a drop-in replacement for the builtin `contr.sum()`. You can set this as the default encoding with

```
options(contrasts=c("named.contr.sum","contr.poly"))
```

This preserves the default coding for ordered factors, but in many cases  `contr.helmert()` might test a more interesting hypothesis. See [here](http://www.ats.ucla.edu/stat/r/library/contrast_coding.htm) for a discussion of coding systems built into R and their interpretation.
